/*! *****************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */

function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

var getUrlPrefix = function () {
    var isProduction = window.location.pathname.includes('/explorer');
    var urlPrefix = isProduction ? '/explorer' : '';
    return urlPrefix;
};

function get(url) {
    console.log(url);
    return new Promise(function (resolve) {
        fetch(url)
            .then(function (response) { return response.json(); })
            .then(function (data) {
            console.log('Received data', data);
            resolve(data);
        });
    });
}

var loadHomePage = function (Vue) {
    console.log('Loading home page...');
    new Vue({
        el: '#homepage',
        data: {
            cycles: [],
            txs: [],
            summary: {
                accounts: {
                    userAccount: 0,
                    nodeAccount: 0,
                    aliasAccount: 0,
                    issueAccount: 0,
                    devIssueAccount: 0,
                    networkAccount: 0,
                },
                txs: {
                    create: 0,
                    register: 0,
                    transfer: 0,
                    message: 0,
                    node_reward: 0
                },
            },
            searchQuery: '',
            totalAccounts: 0,
            totalTxs: 0,
            totalSupply: 0,
            urlPrefix: getUrlPrefix()
        },
        computed: {
            activeNodes: function () {
                if (this.cycles.length === 0)
                    return 0;
                return this.cycles[0].active;
            }
        },
        mounted: function () {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.syncLatestData()];
                        case 1:
                            _a.sent();
                            setInterval(this.syncLatestData, 10000);
                            document.getElementById("modal-close").addEventListener("click", function () {
                                document.getElementById("modal").style.display = "none";
                            });
                            return [2 /*return*/];
                    }
                });
            });
        },
        methods: {
            getTotalAccount: function (blobs, type) {
                var count = blobs
                    .map(function (b) { return b.accByType[type]; })
                    .filter(function (value) { return value > 0; })
                    .reduce(function (a, b) { return a + b; }, 0);
                return count;
            },
            getTotalTxs: function (blobs, type) {
                var count = blobs
                    .map(function (b) { return b.txByType[type]; })
                    .filter(function (value) { return value > 0; })
                    .reduce(function (a, b) { return a + b; }, 0);
                return count;
            },
            onSubmitSearch: function (e) {
                return __awaiter(this, void 0, void 0, function () {
                    var response, _a, cycle, partition;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                e.preventDefault();
                                if (this.searchQuery.length > 8) {
                                    this.truncateSearchString();
                                }
                                return [4 /*yield*/, get(this.urlPrefix + "/api/transaction?txid=" + this.searchQuery)];
                            case 1:
                                response = _b.sent();
                                if (response.transactions && response.transactions.txId) {
                                    _a = response.transactions, cycle = _a.cycle, partition = _a.partition;
                                    console.log(cycle, partition);
                                    window.location.replace(this.urlPrefix + "/partition?cycle=" + cycle + "&partition=" + partition);
                                }
                                else {
                                    console.log('Tx not found');
                                    document.getElementById("modal").style.display = "";
                                }
                                return [2 /*return*/];
                        }
                    });
                });
            },
            onEnterTxId: function (e) {
                if (this.searchQuery.length > 8) {
                    this.truncateSearchString();
                }
            },
            truncateSearchString: function () {
                this.searchQuery = this.searchQuery.substr(0, 8);
            },
            syncLatestData: function () {
                return __awaiter(this, void 0, void 0, function () {
                    var latestTxsResponse, cyclesResponse, accountSummaryResponse, txSummaryResponse, accountBlobs, txBlobs;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, get(this.urlPrefix + "/api/transaction?count=10")];
                            case 1:
                                latestTxsResponse = _a.sent();
                                this.txs = latestTxsResponse.transactions.allTxs.slice(0, 10);
                                return [4 /*yield*/, get(this.urlPrefix + "/api/cycleinfo?count=10")];
                            case 2:
                                cyclesResponse = _a.sent();
                                this.cycles = cyclesResponse.cycles.sort(function (a, b) { return b.counter - a.counter; });
                                return [4 /*yield*/, get(this.urlPrefix + "/api/summary/account")];
                            case 3:
                                accountSummaryResponse = _a.sent();
                                return [4 /*yield*/, get(this.urlPrefix + "/api/summary/tx")];
                            case 4:
                                txSummaryResponse = _a.sent();
                                accountBlobs = accountSummaryResponse.blobs;
                                this.totalSupply = accountBlobs && accountBlobs
                                    .reduce(function (p, c) { return p + c.totalBalance; }, 0);
                                this.summary.accounts.userAccount = accountBlobs && this.getTotalAccount(accountBlobs, 'UserAccount');
                                this.summary.accounts.nodeAccount = accountBlobs && this.getTotalAccount(accountBlobs, 'NodeAccount');
                                this.summary.accounts.aliasAccount = accountBlobs && this.getTotalAccount(accountBlobs, 'AliasAccount');
                                this.summary.accounts.issueAccount = accountBlobs && this.getTotalAccount(accountBlobs, 'IssueAccount');
                                this.summary.accounts.devIssueAccount = accountBlobs && this.getTotalAccount(accountBlobs, 'DevIssueAccount');
                                this.summary.accounts.networkAccount = accountBlobs && this.getTotalAccount(accountBlobs, 'NetworkAccount');
                                txBlobs = txSummaryResponse.blobs;
                                this.summary.txs.create = this.getTotalTxs(txBlobs, 'create');
                                this.summary.txs.register = this.getTotalTxs(txBlobs, 'register');
                                this.summary.txs.transfer = this.getTotalTxs(txBlobs, 'transfer');
                                this.summary.txs.message = this.getTotalTxs(txBlobs, 'message');
                                this.summary.txs.node_reward = this.getTotalTxs(txBlobs, 'node_reward');
                                this.totalAccounts = accountBlobs && accountBlobs.reduce(function (p, c) { return p + c.totalAccounts; }, 0);
                                this.totalTxs = txBlobs && txBlobs.reduce(function (p, c) { return p + c.totalTx; }, 0);
                                return [2 /*return*/];
                        }
                    });
                });
            }
        }
    });
};

var loadCyclePage = function (Vue) {
    console.log('Loading all cycles page...');
    var itemsPerPage = 10;
    new Vue({
        el: '#all-cycles-page',
        data: {
            cycles: [],
            totalPages: 1,
            page: null,
            urlPrefix: getUrlPrefix()
        },
        methods: {
            getLatestCycleCounter: function () {
                var _this = this;
                return new Promise(function (resolve) {
                    fetch(_this.urlPrefix + "/api/cycleinfo?count=1")
                        .then(function (response) { return response.json(); })
                        .then(function (data) {
                        console.log(data);
                        resolve(data.cycles[0].counter);
                    });
                });
            },
            getToAndFrom: function (page) {
                return __awaiter(this, void 0, void 0, function () {
                    var latestCounter, to, from;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, this.getLatestCycleCounter()];
                            case 1:
                                latestCounter = _a.sent();
                                to = latestCounter - (page - 1) * itemsPerPage;
                                from = to - itemsPerPage + 1;
                                // const from = (page - 1) * itemsPerPage + 1
                                // const to = from + itemsPerPage - 1
                                if (from < 0)
                                    from = 0;
                                return [2 /*return*/, { latestCounter: latestCounter, from: from, to: to }];
                        }
                    });
                });
            }
        },
        mounted: function () {
            return __awaiter(this, void 0, void 0, function () {
                var urlParams, page, _a, latestCounter, from, to, totalPages, url;
                var _this = this;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            urlParams = new URLSearchParams(window.location.search);
                            page = parseInt(urlParams.get('page')) || 1;
                            this.page = page;
                            return [4 /*yield*/, this.getToAndFrom(page)];
                        case 1:
                            _a = _b.sent(), latestCounter = _a.latestCounter, from = _a.from, to = _a.to;
                            totalPages = Math.ceil(latestCounter / itemsPerPage);
                            url = this.urlPrefix + "/api/cycleinfo?from=" + from + "&to=" + to;
                            console.log(url);
                            fetch(url)
                                .then(function (response) { return response.json(); })
                                .then(function (data) {
                                _this.cycles = data.cycles.sort(function (a, b) { return b.counter - a.counter; });
                                _this.totalPages = totalPages;
                            });
                            return [2 /*return*/];
                    }
                });
            });
        }
    });
};

var loadCycleDetailPage = function (Vue) {
    console.log('Loading cycle detail page...');
    new Vue({
        el: '#cycle-detail-page',
        data: {
            urlPrefix: getUrlPrefix(),
            archivedCycle: null
        },
        computed: {
            cycle: function () {
                if (this.archivedCycle && this.archivedCycle.cycleRecord) {
                    return this.archivedCycle.cycleRecord;
                }
            }
        },
        mounted: function () {
            return __awaiter(this, void 0, void 0, function () {
                var pathName, counter, archivedCycle;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            pathName = window.location.pathname;
                            counter = pathName.split('/cycle/')[1];
                            return [4 /*yield*/, get(this.urlPrefix + "/api/archive/" + counter)];
                        case 1:
                            archivedCycle = (_a.sent()).archivedCycle;
                            this.archivedCycle = archivedCycle;
                            return [2 /*return*/];
                    }
                });
            });
        },
        methods: {
            getPartitionBlocks: function () {
                return __awaiter(this, void 0, void 0, function () { return __generator(this, function (_a) {
                    return [2 /*return*/];
                }); });
            },
            goPreviousCycle: function () {
                var counter = parseInt(this.cycle.counter) - 1;
                if (counter >= 0)
                    window.location.replace(this.urlPrefix + "/cycle/" + counter);
            },
            goNextCycle: function () {
                return __awaiter(this, void 0, void 0, function () {
                    var counter, latestCounter;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                counter = parseInt(this.cycle.counter) + 1;
                                return [4 /*yield*/, this.getLatestCycleCounter()];
                            case 1:
                                latestCounter = _a.sent();
                                if (counter < latestCounter)
                                    window.location.replace(this.urlPrefix + "/cycle/" + counter);
                                return [2 /*return*/];
                        }
                    });
                });
            },
            getLatestCycleCounter: function () {
                var _this = this;
                return new Promise(function (resolve) {
                    fetch(_this.urlPrefix + "/api/cycleinfo?count=1")
                        .then(function (response) { return response.json(); })
                        .then(function (data) {
                        console.log(data);
                        resolve(data.cycles[0].counter);
                    });
                });
            },
        }
    });
};

var loadPartitionDetailPage = function (Vue) {
    console.log('Loading partition detail page...');
    new Vue({
        el: '#cycle-detail-page',
        data: {
            archivedCycle: null,
            cycle: null,
            partition: null,
            urlPrefix: getUrlPrefix()
        },
        computed: {
            receipts: function () {
                var results = [];
                if (!this.archivedCycle)
                    return results;
                for (var txid in this.archivedCycle.receipt.partitionMaps[this.partition]) {
                    results.push({
                        txid: txid,
                        status: this.archivedCycle.receipt.partitionMaps[this.partition][txid][0]
                    });
                }
                return results;
            }
        },
        mounted: function () {
            return __awaiter(this, void 0, void 0, function () {
                var urlParams, cycle, partition, archivedCycle;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            urlParams = new URLSearchParams(window.location.search);
                            cycle = parseInt(urlParams.get('cycle'));
                            partition = parseInt(urlParams.get('partition'));
                            this.cycle = cycle;
                            this.partition = partition;
                            return [4 /*yield*/, get(this.urlPrefix + "/api/archive/" + cycle)];
                        case 1:
                            archivedCycle = (_a.sent()).archivedCycle;
                            this.archivedCycle = archivedCycle;
                            return [2 /*return*/];
                    }
                });
            });
        }
    });
};

var loadAllTxsPage = function (Vue) {
    console.log('Loading all txs page...');
    var itemsPerPage = 10;
    new Vue({
        el: '#all-cycles-page',
        data: {
            transactions: [],
            page: null,
            totalPages: 1,
            urlPrefix: getUrlPrefix()
        },
        methods: {
            getLatestCycleCounter: function () {
                var _this = this;
                return new Promise(function (resolve) {
                    fetch(_this.urlPrefix + "/api/transaction?count=1")
                        .then(function (response) { return response.json(); })
                        .then(function (data) {
                        console.log(data);
                        if (data.transactions.allTxs.length > 0) {
                            var tx = data.transactions.allTxs[0];
                            resolve(tx.cycle);
                        }
                    });
                });
            },
            getToAndFrom: function (page) {
                return __awaiter(this, void 0, void 0, function () {
                    var latestCounter, to, from;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, this.getLatestCycleCounter()];
                            case 1:
                                latestCounter = _a.sent();
                                to = latestCounter - (page - 1) * itemsPerPage;
                                from = to - itemsPerPage + 1;
                                return [2 /*return*/, { from: from, to: to }];
                        }
                    });
                });
            }
        },
        mounted: function () {
            return __awaiter(this, void 0, void 0, function () {
                var urlParams, page, url;
                var _this = this;
                return __generator(this, function (_a) {
                    urlParams = new URLSearchParams(window.location.search);
                    page = parseInt(urlParams.get('page')) || 1;
                    this.page = page;
                    url = this.urlPrefix + "/api/transaction?page=" + page;
                    fetch(url)
                        .then(function (response) { return response.json(); })
                        .then(function (data) {
                        _this.transactions = data.transactions.allTxs;
                        _this.totalPages = data.totalPages;
                        console.log(_this.transactions);
                    });
                    return [2 /*return*/];
                });
            });
        }
    });
};

var data = {
    loadHomePage: loadHomePage,
    loadCyclePage: loadCyclePage,
    loadCycleDetailPage: loadCycleDetailPage,
    loadPartitionDetailPage: loadPartitionDetailPage,
    loadAllTxsPage: loadAllTxsPage
};
console.log(data);
