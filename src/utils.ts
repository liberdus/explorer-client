import * as Types from './Types'
export const generateHash = function (num) {
  const table = [
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    'a',
    'b',
    'c',
    'd',
    'e',
    'f'
  ]
  let hash = ''
  for (let i = 0; i < num; i++) {
    const randomIndex = Math.floor(Math.random() * table.length)
    hash += table[randomIndex]
  }
  return hash
}

export const genrateFakeTxs = count => {
  //   let result: Map<
  //     Types.TransactionStatus['id'],
  //     Types.TransactionStatus
  //   > = new Map()
  let result = []
  for (let i = 0; i < count; i++) {
    const id = generateHash(64)
    result.push({
      id,
      status: 'approved'
    })
  }
  return result
}

export const generateFakePartitionBlocks = function (
  cycle: number,
  blockCount: number
) {
  let result: Types.PartitionBlock[] = []
  for (let i = 0; i < blockCount; i++) {
    const block: Types.PartitionBlock = {
      partitionId: i,
      cycle,
      startAddr: generateHash(64),
      endAddr: generateHash(64),
      txs: genrateFakeTxs(5),
      data: null
    }
    result.push(block)
  }
  return result
}

export const getUrlPrefix = function (): string {
  const isProduction = window.location.pathname.includes('/explorer')
  const urlPrefix = isProduction ? '/explorer' : ''
  return urlPrefix
}
