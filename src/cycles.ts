import * as Utils from './utils'

export const loadCyclePage = Vue => {
  console.log('Loading all cycles page...')
  const itemsPerPage = 10
  new Vue({
    el: '#all-cycles-page',
    data: {
      cycles: [],
      totalPages: 1,
      page: null,
      urlPrefix: Utils.getUrlPrefix()
    },
    methods: {
      getLatestCycleCounter () {
        return new Promise(resolve => {
          fetch(`${this.urlPrefix}/api/cycleinfo?count=1`)
            .then(response => response.json())
            .then(data => {
              console.log(data)
              resolve(data.cycles[0].counter)
            })
        })
      },
      async getToAndFrom (page) {
        const latestCounter = await this.getLatestCycleCounter()
        const to = latestCounter - (page - 1) * itemsPerPage
        let from = to - itemsPerPage + 1
        // const from = (page - 1) * itemsPerPage + 1
        // const to = from + itemsPerPage - 1
        if (from < 0) from = 0
        return { latestCounter ,from, to }
      }
    },
    mounted: async function () {
      const urlParams = new URLSearchParams(window.location.search)
      let page = parseInt(urlParams.get('page')) || 1
      this.page = page
      const { latestCounter, from, to } = await this.getToAndFrom(page)
      let totalPages = Math.ceil(latestCounter/itemsPerPage);
      let url = `${this.urlPrefix}/api/cycleinfo?from=${from}&to=${to}`
      console.log(url)
      fetch(url)
        .then(response => response.json())
        .then(data => {
          this.cycles = data.cycles.sort((a, b) => b.counter - a.counter)
          this.totalPages = totalPages
        })
    }
  })
}
