export interface Cycle {
  counter: number;
  certificate: string;
  previous: string;
  marker: string;
  start: number;
  duration: number;
  active: number;
  desired: number;
  expired: number;
  syncing: number;
  joined: any;
  joinedArchivers: any;
  leavingArchivers: any;
  joinedConsensors: any;
  refreshedArchivers: any;
  refreshedConsensors: any;
  activated: any;
  activatedPublicKeys: any;
  removed: any;
  returned: any;
  lost: any;
  refuted: any;
  apoptosized: any;
  networkDataHash: any;
  networkReceiptHash: any;
  networkSummaryHash: any;
}

type OpaqueBlob = any;

export type SummaryBlob = {
  cycle: number;
  latestCycle: number; //The highest cycle that was used in this summary.
  counter: number;
  errorNull: number;
  partition: number;
  opaqueBlob: OpaqueBlob;
};

export interface PartitionBlock {
  partitionId: number
  cycle: number
  startAddr: string
  endAddr: string
  txs: TransactionStatus[]
  data: unknown
}

export interface TransactionStatus {
  id: string
  status: string
}

export type ReceiptMap = { [txId: string]: string[] }

export type ReceiptMapResult = {
  cycle: number
  partition: number
  receiptMap: ReceiptMap
  txCount: number
}


type CycleMarker = string

type StateData = {
  parentCycle?: CycleMarker
  networkHash?: string
  partitionHashes?: string[]
}

type Receipt = {
  parentCycle?: CycleMarker
  networkHash?: string
  partitionHashes?: string[]
  partitionMaps?: { [partition: number]: ReceiptMapResult }
  partitionTxs?: { [partition: number]: any }
}

type Summary = {
  parentCycle?: CycleMarker
  networkHash?: string
  partitionHashes?: string[]
  partitionBlobs?: { [partition: number]: SummaryBlob }
}

export class ArchivedCycle {
  cycleRecord!: Cycle
  cycleMarker!: CycleMarker
  data!: StateData
  receipt!: Receipt
  summary!: Summary
}

