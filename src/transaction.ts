import * as Utils from './utils'

export const loadAllTxsPage = Vue => {
  console.log('Loading all txs page...')
  const itemsPerPage = 10
  new Vue({
    el: '#all-cycles-page',
    data: {
      transactions: [],
      page: null,
      totalPages: 1,
      urlPrefix: Utils.getUrlPrefix()
    },
    methods: {
      getLatestCycleCounter () {
        return new Promise(resolve => {
          fetch(`${this.urlPrefix}/api/transaction?count=1`)
            .then(response => response.json())
            .then(data => {
              console.log(data)
              if (data.transactions.allTxs.length > 0) {
                let tx: any = data.transactions.allTxs[0]
                resolve(tx.cycle)
              }
            })
        })
      },
      async getToAndFrom (page) {
        const latestCounter = await this.getLatestCycleCounter()
        const to = latestCounter - (page - 1) * itemsPerPage
        const from = to - itemsPerPage + 1
        return { from, to }
      }
    },
    mounted: async function () {
      const urlParams = new URLSearchParams(window.location.search)
      let page = parseInt(urlParams.get('page')) || 1
      this.page = page
      let url = `${this.urlPrefix}/api/transaction?page=${page}`
      fetch(url)
        .then(response => response.json())
        .then(data => {
          this.transactions = data.transactions.allTxs
          this.totalPages = data.totalPages
          console.log(this.transactions)

        })
    }
  })
}
