import { resolve } from 'path'

export function get (url) {
  console.log(url)
  return new Promise(resolve => {
    fetch(url)
      .then(response => response.json())
      .then(data => {
        console.log('Received data', data)
        resolve(data)
      })
  })
}
