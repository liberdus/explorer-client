import * as Utils from './utils'
import * as http from './http'

export const loadHomePage = Vue => {
  console.log('Loading home page...')
  new Vue({
    el: '#homepage',
    data: {
      cycles: [],
      txs: [],
      summary: {
        accounts: {
          userAccount: 0,
          nodeAccount: 0,
          aliasAccount: 0,
          issueAccount: 0,
          devIssueAccount : 0,
          networkAccount : 0,
        },
        txs: {
          create: 0,
          register: 0,
          transfer: 0,
          message: 0,
          node_reward: 0
        },
      },
      searchQuery: '',
      totalAccounts: 0,
      totalTxs: 0,
      totalSupply: 0,
      urlPrefix: Utils.getUrlPrefix()
    },
    computed: {
      activeNodes: function() {
        if (this.cycles.length === 0) return 0
        return this.cycles[0].active
      }
    },
    mounted: async function () {
      await this.syncLatestData()
      setInterval(this.syncLatestData, 10000)
      document.getElementById("modal-close").addEventListener("click", function () {
          document.getElementById("modal").style.display = "none";
      });
    },
    methods: {
      getTotalAccount (blobs, type) {
        let count = blobs
          .map(b => b.accByType[type])
          .filter(value => value > 0)
          .reduce((a, b) => a + b, 0)
        return count
      },
      getTotalTxs (blobs, type) {
        let count = blobs
          .map(b => b.txByType[type])
          .filter(value => value > 0)
          .reduce((a, b) => a + b, 0)
        return count
      },
      async onSubmitSearch(e) {
        e.preventDefault()
        if (this.searchQuery.length > 8) {
          this.truncateSearchString()
        }
        let response: any = await http.get(
          `${this.urlPrefix}/api/transaction?txid=${this.searchQuery}`
        )
        if (response.transactions && response.transactions.txId) {
          let { cycle, partition } = response.transactions
          console.log(cycle, partition)
          window.location.replace(`${this.urlPrefix}/partition?cycle=${cycle}&partition=${partition}`);
        } else {
          console.log('Tx not found')
          document.getElementById("modal").style.display = "";
        }
      },
      onEnterTxId(e) {
        if (this.searchQuery.length > 8) {
          this.truncateSearchString()
        }
      },
      truncateSearchString() {
        this.searchQuery = this.searchQuery.substr(0, 8)
      },
      async syncLatestData () {
        let latestTxsResponse: any = await http.get(
          `${this.urlPrefix}/api/transaction?count=10`
        )
        this.txs = latestTxsResponse.transactions.allTxs.slice(0, 10)

        let cyclesResponse: any = await http.get(
          `${this.urlPrefix}/api/cycleinfo?count=10`
        )
        this.cycles = cyclesResponse.cycles.sort(
          (a, b) => b.counter - a.counter
        )

        let accountSummaryResponse: any = await http.get(
          `${this.urlPrefix}/api/summary/account`
        )

        let txSummaryResponse: any = await http.get(
          `${this.urlPrefix}/api/summary/tx`
        )

        const accountBlobs = accountSummaryResponse.blobs
        this.totalSupply = accountBlobs && accountBlobs
          .reduce((p, c) => p + c.totalBalance, 0)

        this.summary.accounts.userAccount = accountBlobs && this.getTotalAccount(accountBlobs, 'UserAccount')
        this.summary.accounts.nodeAccount = accountBlobs && this.getTotalAccount(accountBlobs, 'NodeAccount')
        this.summary.accounts.aliasAccount = accountBlobs && this.getTotalAccount(accountBlobs, 'AliasAccount')
        this.summary.accounts.issueAccount = accountBlobs && this.getTotalAccount(accountBlobs, 'IssueAccount')
        this.summary.accounts.devIssueAccount = accountBlobs && this.getTotalAccount(accountBlobs, 'DevIssueAccount')
        this.summary.accounts.networkAccount = accountBlobs && this.getTotalAccount(accountBlobs, 'NetworkAccount')

        const txBlobs = txSummaryResponse.blobs

        this.summary.txs.create = this.getTotalTxs(txBlobs, 'create')
        this.summary.txs.register = this.getTotalTxs(txBlobs, 'register')
        this.summary.txs.transfer = this.getTotalTxs(txBlobs, 'transfer')
        this.summary.txs.message = this.getTotalTxs(txBlobs, 'message')
        this.summary.txs.node_reward = this.getTotalTxs(txBlobs, 'node_reward')

        this.totalAccounts = accountBlobs && accountBlobs.reduce((p, c) => p + c.totalAccounts, 0)
        this.totalTxs = txBlobs && txBlobs.reduce((p, c) => p + c.totalTx, 0)
      }
    }
  })
}
