import * as Types from './Types'
import * as Utils from './utils'
import * as http from './http'

export const loadPartitionDetailPage = Vue => {
  console.log('Loading partition detail page...')
  new Vue({
    el: '#cycle-detail-page',
    data: {
      archivedCycle: null,
      cycle: null,
      partition: null,
      urlPrefix: Utils.getUrlPrefix()
    },
    computed: {
      receipts: function() {
        let results = []
        if (!this.archivedCycle) return results
        for (let txid in this.archivedCycle.receipt.partitionMaps[this.partition]) {
          results.push({
            txid,
            status: this.archivedCycle.receipt.partitionMaps[this.partition][txid][0]
          })
        }
        return results
      }
    },
    mounted: async function () {
      const urlParams = new URLSearchParams(window.location.search)

      const cycle = parseInt(urlParams.get('cycle'))
      const partition = parseInt(urlParams.get('partition'))

      this.cycle = cycle
      this.partition = partition

      const { archivedCycle }: any = await http.get(
        `${this.urlPrefix}/api/archive/${cycle}`
      ) 
      this.archivedCycle = archivedCycle
    }
  })
}
