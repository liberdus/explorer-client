import * as Types from './Types'
import * as Utils from './utils'
import * as http from './http'

export const loadCycleDetailPage = Vue => {
  console.log('Loading cycle detail page...')
  new Vue({
    el: '#cycle-detail-page',
    data: {
      urlPrefix: Utils.getUrlPrefix(),
      archivedCycle: null
    },
    computed: {
      cycle() {
        if (this.archivedCycle && this.archivedCycle.cycleRecord) {
          return this.archivedCycle.cycleRecord
        }
      }
    },
    mounted: async function () {
      const pathName = window.location.pathname
      const counter = pathName.split('/cycle/')[1]
      const { archivedCycle }: any = await http.get(
        `${this.urlPrefix}/api/archive/${counter}`
      ) 
      this.archivedCycle = archivedCycle
    },
    methods: {
      async getPartitionBlocks () {},
      goPreviousCycle() {
        let counter = parseInt(this.cycle.counter) - 1
        if(counter >= 0) window.location.replace(`${this.urlPrefix}/cycle/${counter}`);
      },
      async goNextCycle() {
        const counter = parseInt(this.cycle.counter) + 1
        const latestCounter = await this.getLatestCycleCounter();
        if(counter < latestCounter) window.location.replace(`${this.urlPrefix}/cycle/${counter}`);
      },
      getLatestCycleCounter () {
        return new Promise(resolve => {
          fetch(`${this.urlPrefix}/api/cycleinfo?count=1`)
            .then(response => response.json())
            .then(data => {
              console.log(data)
              resolve(data.cycles[0].counter)
            })
        })
      },
    }
  })
}
