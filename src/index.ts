import { loadHomePage } from './home'
import { loadCyclePage } from './cycles'
import { loadCycleDetailPage } from './cycleDetail'
import { loadPartitionDetailPage } from './partition'
import { loadAllTxsPage } from './transaction'

let data = {
  loadHomePage,
  loadCyclePage,
  loadCycleDetailPage,
  loadPartitionDetailPage,
  loadAllTxsPage
}

console.log(data)
