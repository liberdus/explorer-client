import typescript from '@rollup/plugin-typescript'
import { nodeResolve } from '@rollup/plugin-node-resolve'

export default {
  input: 'src/index.ts',
  output: {
    dir: 'public/js',
    format: 'es'
  },
  plugins: [
    typescript({ lib: ['es5', 'es6', 'dom'], target: 'es5' }),
    nodeResolve()
  ]
}
