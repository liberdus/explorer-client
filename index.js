let express = require('express')
let app = express()
// set the view engine to ejs
app.set('views', __dirname + '/views')
app.set('view engine', 'ejs')
app.use('/public', express.static(__dirname + '/public'))

// use res.render to load up an ejs view file

// index page
app.get('', function (req, res) {
  res.render('index')
})

// all cycles page
app.get('/cycle', function (req, res) {
  res.render('cycles')
})

// all cycles txs
app.get('/transaction', function (req, res) {
  res.render('transactions')
})

// cycle detail page
app.get('/cycle/:counter', function (req, res) {
  res.render('cycledetail')
})

// cycle detail page
app.get('/partition', function (req, res) {
  res.render('partition')
})

// cycle detail page
app.get('/api/*', function (req, res) {
  const url = req.url.replace('','')
  // res.redirect(`http://localhost:4444${url}`)
  res.redirect(`https://dev.liberdus.com/explorer${url}`)
})

app.listen(8080)
console.log('Dev server is running at port 8080')
